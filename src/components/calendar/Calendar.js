import React, { useState, useEffect } from "react";
import moment from "moment";
import "./Calendar.css";

export const Calendar = () => {
  const [dateObject] = useState(moment());
  const [calendarRows, setCalendarRows] = useState([]);

  const getFirstWeekDayOfMonth = () => {
    let firstDay = moment(dateObject).startOf("month").format("d");
    return firstDay;
  };

  const getCurrentMonth = () => {
    return dateObject.format("MMM");
  };

  const generateCalendar = () => {
    //create blank area
    let blanks = [];
    for (let i = 0; i < getFirstWeekDayOfMonth(); i++) {
      blanks.push(<td className="calendar-day empty">{""}</td>);
    }

    //generate days of the week
    let daysInMonth = [];
    for (let d = 1; d <= moment().daysInMonth(); d++) {
      //dateObject.day() gets the current day from moment object
      let currentDay = d === dateObject.day() ? "today" : "";

      daysInMonth.push(
        <td key={d} className={`calendar-day ${currentDay}`}>
          {d}
        </td>
      );
    }

    var totalSlots = [...blanks, ...daysInMonth];
    let rows = [];
    let cells = [];

    totalSlots.forEach((row, i) => {
      if (i % 7 !== 0) {
        cells.push(row); // if index not equal 7 that means not go to next week
      } else {
        rows.push(cells); // when reach next week we contain all td in last week to rows
        cells = []; // empty container
        cells.push(row); // in current loop we still push current row to new container
      }
      if (i === totalSlots.length - 1) {
        // when end loop we add remain date
        rows.push(cells);
      }
    });

    setCalendarRows(rows);
  };

  useEffect(() => {
    generateCalendar();
  }, []);

  //generateCalendar();

  let weekdayshortname = moment.weekdaysShort();
  return (
    <>
      <div>
        <h2>Calendar</h2>
      </div>
      <div>
        <h3>{getCurrentMonth()}</h3>
      </div>
      <table className="calendar-day">
        <thead>
          {weekdayshortname.map((day) => {
            return (
              <th key={day} className="week-day">
                {day}
              </th>
            );
          })}
        </thead>
        <tbody>
          {calendarRows.map((day) => {
            return <tr>{day}</tr>;
          })}
        </tbody>
      </table>
    </>
  );
};
